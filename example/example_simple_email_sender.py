from simple_email_sender import Sender

# Before running this example, you need to change your settings in the
# 'server_settings.yaml'-file according to your personal needs


def main():
    receiver = "example@email.com"
    sender = Sender("./server_settings.yaml", receiver, subject="Experiment 123")
    sender.info("Hello World")
    sender.error("Something unexpected happened and you should take care of it...")


if __name__ == "__main__":
    main()
