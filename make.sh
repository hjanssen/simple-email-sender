#!/usr/bin/env bash

# Shell script used as a "Makefile-like" helper tool.
# This script can be run using a standard installation of Git on Windows in the Git-BASH.

# ------------------------------------
# for loop over all keywords given
# ------------------------------------

for KEYWORD in "$@"
do
    case "${KEYWORD}"
    in
        "help")
            # print help
            echo "This is a small helper script to trigger frequently used command sets. Usage:"
            echo "./make.sh keyword1 keyword2 ..."
            echo "Multiple keywords are executed in the sequence given."
            echo "The virtual environment has to be active."
            echo ""
            echo "build:"
            echo "    Build the package as source and wheel."
            echo ""
            ;;

        "build_wheel")
            python setup.py bdist_wheel
            ;;

        "build_source")
            python setup.py sdist
            ;;

        "build")
            python setup.py sdist bdist_wheel
            ;;

        *)
            # default action if keyword not recognized
            echo "Keyword '${KEYWORD}' is not recognized."
            echo "Use './make.sh help' to get a list of possibilities"
    esac
done
